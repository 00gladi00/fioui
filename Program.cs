﻿#define USING_URI

using System;
using System.Windows.Forms;

using Microsoft.Win32;

namespace FIOUI
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			SetupRegistry();

			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);

#if USING_URI
			if ( args.Length == 2 && args[0].ToUpper() == "/URI")
			{
				string fioLink = args[1];
				Application.Run(new Main(fioLink));
				return;
			}
#endif
			Application.Run(new Main());
		}

		internal static void SetupRegistry()
		{
#if USING_URI
			// Not there yet
			try
			{
				// Retrieve ico path
				const string fioClassesApplicationRegPath = @"Software\Classes\Applications\FIOUI.exe";
				RegistryKey appRegKey = Registry.CurrentUser.CreateSubKey(fioClassesApplicationRegPath);

				string ExePath = System.Reflection.Assembly.GetExecutingAssembly().Location;

				const string fioClassRegPath = @"Software\Classes\fio";
				RegistryKey classesFio = Registry.CurrentUser.CreateSubKey(fioClassRegPath);
				classesFio.SetValue(String.Empty, "URL:FIOUI", RegistryValueKind.String);
				classesFio.SetValue("URL Protocol", String.Empty, RegistryValueKind.String);

				const string fioShellCommandRegPath = @"Software\Classes\fio\shell\open\command";
				RegistryKey classesFioShellCommand = Registry.CurrentUser.CreateSubKey(fioShellCommandRegPath);
				string commandString = String.Format("\"{0}\" /URI \"%1\"", ExePath);
				classesFioShellCommand.SetValue(String.Empty, commandString, RegistryValueKind.String);
			}
			catch (Exception)
			{
				// No permission, ignore
			}
#endif
		}
	}
}
