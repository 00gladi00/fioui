﻿//#define USE_LOCALHOST

namespace FIOUI.Web
{
    public static class WebConsts
    {
#if USE_LOCALHOST
        public const string RootUrl = "http://localhost:4443";
#else
        public const string RootUrl = "https://rest.fnar.net";
#endif
    }
}
