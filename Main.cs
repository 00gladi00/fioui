﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

using FIOUI.Dialogs;
using FIOUI.Tabs;
using FIOUI.Tabs.BuildingDataTab;
using FIOUI.Tabs.BurnRateTab;
using FIOUI.Tabs.InventoryTab;
using FIOUI.Tabs.PermissionsTab;
using FIOUI.Tabs.PlanetSearchTab;
using FIOUI.Tabs.ShipmentFinderTab;

using FIOUI.Web;

using WeifenLuo.WinFormsUI.Docking;

namespace FIOUI
{
    public partial class Main : DockContent
    {
        protected TaskScheduler UISyncContext = null;

        public static Authentication Auth = null;
        private bool IsAdmin = false;

        public Main(string fioLink = null)
        {
            InitializeComponent();

            UISyncContext = TaskScheduler.FromCurrentSynchronizationContext();
        }

        private async void Main_Load(object sender, EventArgs e)
        {
            this.ReadSettings();

            Auth = new Authentication(Settings.UserName);
            bool bLoggedIn = await Auth.PromptLogin();
            if(bLoggedIn)
            {
                this.RestoreSettings();
                if (!dockPanel.RestoreSettings())
                {
                    CreateDefaultTabs();
                }

                PopulateMenuItems();

                await CheckAdmin();
                _ = CheckForUpdate();
            }
            else
            {
                MessageBox.Show("Failed to login.");
                Close();
            }
        }

        private void PopulateMenuItems()
        {
            var SupportedFIOTabTypes = from t in Assembly.GetExecutingAssembly().GetTypes() where t.IsSubclassOf(typeof(FIOTab)) select t;
            foreach (var supported in SupportedFIOTabTypes)
            {
                // Add all tabs as options to the Windows menu
                string tabName = supported.Name.Replace("Tab", "");
                ToolStripMenuItem item = new ToolStripMenuItem(tabName, null, (s, e) =>
                {
                    FIOTab existingTab = FIOTab.OpenFIOTabs.Where(oft => oft.DataUserName == Auth.UserName && oft.GetType() == supported).FirstOrDefault();
                    if (existingTab != null)
                    {
                        existingTab.Activate();
                    }
                    else
                    {
                        var ourTabInstance = (FIOTab)Activator.CreateInstance(supported);
                        ourTabInstance.Show(dockPanel, DockState.Document);
                        _ = ourTabInstance.RefreshDataAsync();
                    }
                });
                windowToolStripMenuItem.DropDownItems.Insert(1, item);

                var instance = (FIOTab)Activator.CreateInstance(supported);
                if ( instance.CanUseWithOtherUsers )
                {
                    ToolStripMenuItem otherUserItem = new ToolStripMenuItem(tabName, null, async (s, e) =>
                    {
                        instance = (FIOTab)Activator.CreateInstance(supported);
                        if ( await instance.PreCreateCheck() )
                        {
                            string selectedUserName = instance.DataUserName;
                            FIOTab existingTab = FIOTab.OpenFIOTabs.Where(oft => oft.DataUserName == selectedUserName && oft.GetType() == instance.GetType()).FirstOrDefault();
                            if (existingTab != null)
                            {
                                existingTab.Activate();
                            }
                            else
                            {
                                instance.Show(dockPanel, DockState.Document);
                                _ = instance.RefreshDataAsync();
                            }
                        }
                    });

                    createNewWindowToolStripMenuItem.DropDownItems.Add(otherUserItem);
                }
            }
        }

        private void CreateDefaultTabs()
        {
            PermissionsTab permissionTab = new PermissionsTab();
            permissionTab.Show(dockPanel, DockState.Document);
            _ = permissionTab.RefreshDataAsync();

            ShipmentFinderTab shipmentFinderTab = new ShipmentFinderTab();
            shipmentFinderTab.Show(dockPanel, DockState.Document);
            _ = shipmentFinderTab.RefreshDataAsync();

            BurnRateTab workforceTab = new BurnRateTab();
            workforceTab.Show(dockPanel, DockState.Document);
            _ = workforceTab.RefreshDataAsync();

            BuildingDataTab buildingDataTab = new BuildingDataTab();
            buildingDataTab.Show(dockPanel, DockState.Document);
            _ = buildingDataTab.RefreshDataAsync();

            InventoryTab inventoryTab = new InventoryTab();
            inventoryTab.Show(dockPanel, DockState.Document);
            _ = inventoryTab.RefreshDataAsync();

            PlanetSearchTab planetSearchTab = new PlanetSearchTab();
            planetSearchTab.Show(dockPanel, DockState.Document);
            _ = planetSearchTab.RefreshDataAsync();
        }

        private async Task CheckAdmin()
        {
            using (GetRequest checkAdmin = new GetRequest("/admin", Auth.AuthToken))
            {
                await checkAdmin.GetResultNoResponseAsync();
                IsAdmin = (checkAdmin.StatusCode == HttpStatusCode.OK);
                createAccountToolStripMenuItem.Visible = IsAdmin;
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void changePasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangePasswordDlg changePwDlg = new ChangePasswordDlg();
            changePwDlg.ShowDialog();
        }

        private void createAccountToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateAccountDlg createAccountDlg = new CreateAccountDlg();
            createAccountDlg.ShowDialog();
        }

        private void refreshAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach( FIOTab tab in FIOTab.OpenFIOTabs)
            {
                _ = tab.RefreshDataAsync();
            }
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            if ( Auth.IsAuthenticated )
            {
                dockPanel.SaveSettings();
                this.SaveSettings();
            }
        }

        private void updateAvailableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UpdateAvailableDlg updateAvailDlg = new UpdateAvailableDlg();
            updateAvailDlg.ShowDialog();
        }

        private async Task CheckForUpdate()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            string currentVersion = fvi.FileVersion;

            string latestVersion = null;
            using (GetRequest getVersion = new GetRequest("/version/latest"))
            {
                latestVersion = await getVersion.GetResultAsStringAsync();
                latestVersion = latestVersion.Trim();
            }

            if (latestVersion != null && currentVersion != latestVersion)
            {
                await RunOnUIThread(() =>
                {
                    helpToolStripMenuItem.Text = "Update Available";
                    helpToolStripMenuItem.Font = new Font(helpToolStripMenuItem.Font, FontStyle.Bold);
                    updateAvailableToolStripMenuItem.Text = "Get Update";
                });
            }
        }

        private Task RunOnUIThread(Action action)
        {
            return Task.Factory.StartNew(action, CancellationToken.None, TaskCreationOptions.None, UISyncContext);
        }
    }
}
