﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace FIOUI.Dialogs
{
    public partial class DropboxDlg : Form
    {
        public string SelectedOption
        {
            get; private set;
        } = null;

        public DropboxDlg(string Title, List<string> DropdownOptions)
        {
            InitializeComponent();

            Text = Title;

            comboBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            comboBox.AutoCompleteSource = AutoCompleteSource.ListItems;

            comboBox.Items.AddRange(DropdownOptions.ToArray());
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            int selectedIdx = comboBox.SelectedIndex;
            if ( selectedIdx >= 0)
            {
                SelectedOption = comboBox.Items[selectedIdx] as string;
            }

            Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            SelectedOption = null;

            Close();
        }
    }
}
