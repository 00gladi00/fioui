﻿using System;
using System.Windows.Forms;

namespace FIOUI.Dialogs
{
    public partial class LoginDlg : Form
    {
        public string UserName
        {
            get; private set;
        }

        public string Password
        {
            get; private set;
        }

        public LoginDlg(string UserName = "")
        {
            InitializeComponent();
            if (!String.IsNullOrWhiteSpace(UserName))
            {
                usernameTextBox.Text = UserName;
            }
        }

        private void logInButton_Click(object sender, EventArgs e)
        {
            UserName = usernameTextBox.Text;
            Password = passwordTextBox.Text;

            DialogResult = DialogResult.OK;
            Close();
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void LoginDlg_Load(object sender, EventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(usernameTextBox.Text))
            {
                ActiveControl = passwordTextBox;
            }
        }
    }
}
