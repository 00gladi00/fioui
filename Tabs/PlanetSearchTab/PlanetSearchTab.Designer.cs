﻿
namespace FIOUI.Tabs.PlanetSearchTab
{
    partial class PlanetSearchTab
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PlanetSearchTab));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.materialsCheckedListBox = new System.Windows.Forms.CheckedListBox();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.mustHavePlanetPropertiesCheckedListBox = new System.Windows.Forms.CheckedListBox();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.allowedPlanetSettingsCheckedListBox = new System.Windows.Forms.CheckedListBox();
            this.searchButton = new System.Windows.Forms.Button();
            this.materialsObjectListView = new BrightIdeasSoftware.ObjectListView();
            this.resourceMaterialColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.resourceTypeColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.concentrationColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.dailyExtractionColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.planetSearchResultsObjectListView = new BrightIdeasSoftware.ObjectListView();
            this.displayNameColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.materialSummaryColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.fertilityColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.surfaceTypeColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.gravityColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.pressureColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.temperatureColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.localMarketColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.chamberOfCommerceColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.warehouseColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.administrationCenterColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.materialsObjectListView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.planetSearchResultsObjectListView)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.planetSearchResultsObjectListView, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 165F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1278, 626);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 172F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 157F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 166F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 91F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 686F));
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel2, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel3, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.searchButton, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.materialsObjectListView, 4, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1272, 159);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.label1);
            this.flowLayoutPanel1.Controls.Add(this.materialsCheckedListBox);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(166, 153);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Margin = new System.Windows.Forms.Padding(3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Must Have Materials:";
            // 
            // materialsCheckedListBox
            // 
            this.materialsCheckedListBox.CheckOnClick = true;
            this.materialsCheckedListBox.FormattingEnabled = true;
            this.materialsCheckedListBox.Location = new System.Drawing.Point(3, 22);
            this.materialsCheckedListBox.Name = "materialsCheckedListBox";
            this.materialsCheckedListBox.Size = new System.Drawing.Size(151, 124);
            this.materialsCheckedListBox.TabIndex = 1;
            this.materialsCheckedListBox.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.materialsCheckedListBox_ItemCheck);
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.label2);
            this.flowLayoutPanel2.Controls.Add(this.mustHavePlanetPropertiesCheckedListBox);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(175, 3);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(151, 153);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 3);
            this.label2.Margin = new System.Windows.Forms.Padding(3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Must Have Planet Settings:";
            // 
            // mustHavePlanetPropertiesCheckedListBox
            // 
            this.mustHavePlanetPropertiesCheckedListBox.CheckOnClick = true;
            this.mustHavePlanetPropertiesCheckedListBox.FormattingEnabled = true;
            this.mustHavePlanetPropertiesCheckedListBox.Items.AddRange(new object[] {
            "Fertile",
            "Local Market",
            "Chamber of Commerce",
            "Warehouse",
            "Administration Center"});
            this.mustHavePlanetPropertiesCheckedListBox.Location = new System.Drawing.Point(3, 22);
            this.mustHavePlanetPropertiesCheckedListBox.Name = "mustHavePlanetPropertiesCheckedListBox";
            this.mustHavePlanetPropertiesCheckedListBox.Size = new System.Drawing.Size(138, 124);
            this.mustHavePlanetPropertiesCheckedListBox.TabIndex = 4;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.label3);
            this.flowLayoutPanel3.Controls.Add(this.allowedPlanetSettingsCheckedListBox);
            this.flowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(332, 3);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(160, 153);
            this.flowLayoutPanel3.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 3);
            this.label3.Margin = new System.Windows.Forms.Padding(3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(132, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Can Have Planet Settings:";
            // 
            // allowedPlanetSettingsCheckedListBox
            // 
            this.allowedPlanetSettingsCheckedListBox.CheckOnClick = true;
            this.allowedPlanetSettingsCheckedListBox.FormattingEnabled = true;
            this.allowedPlanetSettingsCheckedListBox.Items.AddRange(new object[] {
            "Rocky (MCG)",
            "Gaseous (AEF)",
            "Low Gravity (MGC)",
            "High Gravity (BL)",
            "Low Pressure (SEA)",
            "High Pressure (HSE)",
            "Low Temperature (INF)",
            "High Temperature (TSH)"});
            this.allowedPlanetSettingsCheckedListBox.Location = new System.Drawing.Point(3, 22);
            this.allowedPlanetSettingsCheckedListBox.Name = "allowedPlanetSettingsCheckedListBox";
            this.allowedPlanetSettingsCheckedListBox.Size = new System.Drawing.Size(141, 124);
            this.allowedPlanetSettingsCheckedListBox.TabIndex = 4;
            // 
            // searchButton
            // 
            this.searchButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.searchButton.Location = new System.Drawing.Point(498, 133);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(75, 23);
            this.searchButton.TabIndex = 3;
            this.searchButton.Text = "Search";
            this.searchButton.UseVisualStyleBackColor = true;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // materialsObjectListView
            // 
            this.materialsObjectListView.AllColumns.Add(this.resourceMaterialColumn);
            this.materialsObjectListView.AllColumns.Add(this.resourceTypeColumn);
            this.materialsObjectListView.AllColumns.Add(this.concentrationColumn);
            this.materialsObjectListView.AllColumns.Add(this.dailyExtractionColumn);
            this.materialsObjectListView.AlternateRowBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.materialsObjectListView.CellEditUseWholeCell = false;
            this.materialsObjectListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.resourceMaterialColumn,
            this.resourceTypeColumn,
            this.concentrationColumn,
            this.dailyExtractionColumn});
            this.materialsObjectListView.Cursor = System.Windows.Forms.Cursors.Default;
            this.materialsObjectListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.materialsObjectListView.FullRowSelect = true;
            this.materialsObjectListView.HideSelection = false;
            this.materialsObjectListView.Location = new System.Drawing.Point(589, 3);
            this.materialsObjectListView.Name = "materialsObjectListView";
            this.materialsObjectListView.ShowGroups = false;
            this.materialsObjectListView.Size = new System.Drawing.Size(680, 153);
            this.materialsObjectListView.TabIndex = 4;
            this.materialsObjectListView.UseAlternatingBackColors = true;
            this.materialsObjectListView.UseCellFormatEvents = true;
            this.materialsObjectListView.UseCompatibleStateImageBehavior = false;
            this.materialsObjectListView.View = System.Windows.Forms.View.Details;
            this.materialsObjectListView.FormatCell += new System.EventHandler<BrightIdeasSoftware.FormatCellEventArgs>(this.materialsObjectListView_FormatCell);
            // 
            // resourceMaterialColumn
            // 
            this.resourceMaterialColumn.AspectName = "Material";
            this.resourceMaterialColumn.Text = "Resource";
            this.resourceMaterialColumn.Width = 81;
            // 
            // resourceTypeColumn
            // 
            this.resourceTypeColumn.AspectName = "ResourceType";
            this.resourceTypeColumn.Text = "Type";
            this.resourceTypeColumn.Width = 79;
            // 
            // concentrationColumn
            // 
            this.concentrationColumn.AspectName = "Concentration";
            this.concentrationColumn.AspectToStringFormat = "{0:N2}";
            this.concentrationColumn.Text = "Concentration";
            this.concentrationColumn.Width = 92;
            // 
            // dailyExtractionColumn
            // 
            this.dailyExtractionColumn.AspectName = "DailyExtraction";
            this.dailyExtractionColumn.AspectToStringFormat = "{0:N2}";
            this.dailyExtractionColumn.Text = "Daily Extraction";
            this.dailyExtractionColumn.Width = 96;
            // 
            // planetSearchResultsObjectListView
            // 
            this.planetSearchResultsObjectListView.AllColumns.Add(this.displayNameColumn);
            this.planetSearchResultsObjectListView.AllColumns.Add(this.materialSummaryColumn);
            this.planetSearchResultsObjectListView.AllColumns.Add(this.fertilityColumn);
            this.planetSearchResultsObjectListView.AllColumns.Add(this.surfaceTypeColumn);
            this.planetSearchResultsObjectListView.AllColumns.Add(this.gravityColumn);
            this.planetSearchResultsObjectListView.AllColumns.Add(this.pressureColumn);
            this.planetSearchResultsObjectListView.AllColumns.Add(this.temperatureColumn);
            this.planetSearchResultsObjectListView.AllColumns.Add(this.localMarketColumn);
            this.planetSearchResultsObjectListView.AllColumns.Add(this.chamberOfCommerceColumn);
            this.planetSearchResultsObjectListView.AllColumns.Add(this.warehouseColumn);
            this.planetSearchResultsObjectListView.AllColumns.Add(this.administrationCenterColumn);
            this.planetSearchResultsObjectListView.AlternateRowBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.planetSearchResultsObjectListView.CellEditUseWholeCell = false;
            this.planetSearchResultsObjectListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.displayNameColumn,
            this.materialSummaryColumn,
            this.fertilityColumn,
            this.surfaceTypeColumn,
            this.gravityColumn,
            this.pressureColumn,
            this.temperatureColumn,
            this.localMarketColumn,
            this.chamberOfCommerceColumn,
            this.warehouseColumn,
            this.administrationCenterColumn});
            this.planetSearchResultsObjectListView.Cursor = System.Windows.Forms.Cursors.Default;
            this.planetSearchResultsObjectListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.planetSearchResultsObjectListView.FullRowSelect = true;
            this.planetSearchResultsObjectListView.HideSelection = false;
            this.planetSearchResultsObjectListView.Location = new System.Drawing.Point(3, 168);
            this.planetSearchResultsObjectListView.Name = "planetSearchResultsObjectListView";
            this.planetSearchResultsObjectListView.ShowGroups = false;
            this.planetSearchResultsObjectListView.ShowImagesOnSubItems = true;
            this.planetSearchResultsObjectListView.Size = new System.Drawing.Size(1272, 455);
            this.planetSearchResultsObjectListView.TabIndex = 1;
            this.planetSearchResultsObjectListView.UseAlternatingBackColors = true;
            this.planetSearchResultsObjectListView.UseCellFormatEvents = true;
            this.planetSearchResultsObjectListView.UseCompatibleStateImageBehavior = false;
            this.planetSearchResultsObjectListView.View = System.Windows.Forms.View.Details;
            this.planetSearchResultsObjectListView.FormatCell += new System.EventHandler<BrightIdeasSoftware.FormatCellEventArgs>(this.planetSearchResultsObjectListView_FormatCell);
            this.planetSearchResultsObjectListView.SelectionChanged += new System.EventHandler(this.planetSearchResultsObjectListView_SelectionChanged);
            // 
            // displayNameColumn
            // 
            this.displayNameColumn.AspectName = "DisplayName";
            this.displayNameColumn.Text = "Planet";
            this.displayNameColumn.Width = 120;
            // 
            // materialSummaryColumn
            // 
            this.materialSummaryColumn.AspectName = "MaterialSummary";
            this.materialSummaryColumn.Text = "Materials";
            this.materialSummaryColumn.Width = 205;
            // 
            // fertilityColumn
            // 
            this.fertilityColumn.AspectName = "Fertility";
            this.fertilityColumn.AspectToStringFormat = "{0:N2}%";
            this.fertilityColumn.Text = "Fertility";
            this.fertilityColumn.Width = 70;
            // 
            // surfaceTypeColumn
            // 
            this.surfaceTypeColumn.AspectName = "SurfaceType";
            this.surfaceTypeColumn.Text = "Type";
            this.surfaceTypeColumn.Width = 84;
            // 
            // gravityColumn
            // 
            this.gravityColumn.AspectName = "Gravity";
            this.gravityColumn.Text = "Gravity";
            this.gravityColumn.Width = 53;
            // 
            // pressureColumn
            // 
            this.pressureColumn.AspectName = "Pressure";
            this.pressureColumn.Text = "Pressure";
            // 
            // temperatureColumn
            // 
            this.temperatureColumn.AspectName = "Temperature";
            this.temperatureColumn.Text = "Temperature";
            this.temperatureColumn.Width = 74;
            // 
            // localMarketColumn
            // 
            this.localMarketColumn.AspectName = "HasLocalMarket";
            this.localMarketColumn.Text = "Local Market";
            this.localMarketColumn.Width = 79;
            // 
            // chamberOfCommerceColumn
            // 
            this.chamberOfCommerceColumn.AspectName = "HasChamberOfCommerce";
            this.chamberOfCommerceColumn.Text = "COGC";
            // 
            // warehouseColumn
            // 
            this.warehouseColumn.AspectName = "HasWarehouse";
            this.warehouseColumn.Text = "Warehouse";
            this.warehouseColumn.Width = 83;
            // 
            // administrationCenterColumn
            // 
            this.administrationCenterColumn.AspectName = "HasAdministrationCenter";
            this.administrationCenterColumn.Text = "Admin Center";
            this.administrationCenterColumn.Width = 78;
            // 
            // PlanetSearchTab
            // 
            this.AcceptButton = this.searchButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1278, 626);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PlanetSearchTab";
            this.Text = "PlanetSearchTab";
            this.Load += new System.EventHandler(this.PlanetSearchTab_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.materialsObjectListView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.planetSearchResultsObjectListView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckedListBox materialsCheckedListBox;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Button searchButton;
        private BrightIdeasSoftware.ObjectListView planetSearchResultsObjectListView;
        private BrightIdeasSoftware.OLVColumn displayNameColumn;
        private BrightIdeasSoftware.OLVColumn fertilityColumn;
        private BrightIdeasSoftware.OLVColumn surfaceTypeColumn;
        private BrightIdeasSoftware.OLVColumn gravityColumn;
        private BrightIdeasSoftware.OLVColumn pressureColumn;
        private BrightIdeasSoftware.OLVColumn temperatureColumn;
        private BrightIdeasSoftware.OLVColumn localMarketColumn;
        private BrightIdeasSoftware.OLVColumn chamberOfCommerceColumn;
        private BrightIdeasSoftware.OLVColumn warehouseColumn;
        private BrightIdeasSoftware.OLVColumn administrationCenterColumn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckedListBox mustHavePlanetPropertiesCheckedListBox;
        private System.Windows.Forms.CheckedListBox allowedPlanetSettingsCheckedListBox;
        private System.Windows.Forms.Label label3;
        private BrightIdeasSoftware.ObjectListView materialsObjectListView;
        private BrightIdeasSoftware.OLVColumn resourceMaterialColumn;
        private BrightIdeasSoftware.OLVColumn resourceTypeColumn;
        private BrightIdeasSoftware.OLVColumn concentrationColumn;
        private BrightIdeasSoftware.OLVColumn dailyExtractionColumn;
        private BrightIdeasSoftware.OLVColumn materialSummaryColumn;
    }
}