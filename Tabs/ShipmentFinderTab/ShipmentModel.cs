﻿namespace FIOUI.Tabs.ShipmentFinderTab
{
    public class ShipmentModel
    {
        public string PlanetId { get; set; }
        public string PlanetNaturalId { get; set; }
        public string PlanetName { get; set; }

        public string ContractPlanetDisplayName
        {
            get
            {
                return (PlanetNaturalId != PlanetName) ? $"{PlanetName} ({PlanetNaturalId})" : PlanetNaturalId;
            }
        }

        public string OriginPlanetId { get; set; }
        public string OriginPlanetNaturalId { get; set; }
        public string OriginPlanetName { get; set; }

        public string OriginPlanetDisplayName
        {
            get
            {
                return (OriginPlanetNaturalId != OriginPlanetName) ? $"{OriginPlanetName} ({OriginPlanetNaturalId})" : OriginPlanetNaturalId;
            }
        }

        public string DestinationPlanetId { get; set; }
        public string DestinationPlanetNaturalId { get; set; }
        public string DestinationPlanetName { get; set; }

        public string DestinationPlanetDisplayName
        {
            get
            {
                return (DestinationPlanetNaturalId != DestinationPlanetName) ? $"{DestinationPlanetName} ({DestinationPlanetNaturalId})" : DestinationPlanetNaturalId;
            }
        }

        public double CargoWeight { get; set; }
        public string CargoWeightDisplay
        {
            get
            {
                return CargoWeight.ToString("N2") + "t";
            }
        }

        public double CargoVolume { get; set; }
        public string CargoVolumeDisplay
        {
            get
            {
                return CargoVolume.ToString("N2") + "m³";
            }
        }

        public string CreatorCompanyId { get; set; }
        public string CreatorCompanyName { get; set; }
        public string CreatorCompanyCode { get; set; }

        public double PayoutPrice { get; set; }
        public string PayoutCurrency { get; set; }
        public string PayoutDisplay
        {
            get
            {
                return PayoutPrice.ToString("N2") + $" {PayoutCurrency}";
            }
        }

        public double PayoutPricePer400Ton
        {
            get
            {
                return 400.0 / CargoWeight * PayoutPrice;
            }
        }
        public double PayoutPricePer400MeterSquared
        {
            get
            {
                return 400.0 / CargoVolume * PayoutPrice;
            }
        }
        public double PayoutPricePer400
        {
            get
            {
                return (CargoWeight >= CargoVolume) ? PayoutPricePer400Ton : PayoutPricePer400MeterSquared;
            }
        }
        public string PayoutPricePer400UnitsDisplay
        {
            get
            {
                if ( CargoWeight >= CargoVolume)
                {
                    string ppt = PayoutPricePer400Ton.ToString("N2");
                    return $"{ppt} {PayoutCurrency}/400t";
                }
                else
                {
                    string ppv = PayoutPricePer400MeterSquared.ToString("N2");
                    return $"{ppv} {PayoutCurrency}/400m³";
                }
            }
        }

        public int DeliveryTime { get; set; }
        public string DeliveryTimeDisplay
        {
            get
            {
                string suffix = DeliveryTime > 1 ? "s" : "";
                return $"{DeliveryTime} day{suffix}";
            }
        }

        public long CreationTimeEpochMs { get; set; }
        public long ExpiryTimeEpochMs { get; set; }

        public string MinimumRating { get; set; }
        public string RatingDisplay
        {
            get
            {
                if ( MinimumRating == "PENDING")
                {
                    return "P";
                }
                else
                {
                    return MinimumRating;
                }
            }
        }
    }
}
