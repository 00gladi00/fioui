﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Forms;

using BrightIdeasSoftware;

using FIOUI.Web;

namespace FIOUI.Tabs.ShipmentFinderTab
{
    public partial class ShipmentFinderTab : FIOTab
    {
        private List<ShipmentModel> ShipmentModels = null;

        public ShipmentFinderTab()
        {
            InitializeComponent();

            shipmentFinderObjectListView.CustomSorter = delegate (OLVColumn column, SortOrder order) 
            {
                shipmentFinderObjectListView.ListViewItemSorter = new ShipmentModelSorter(order, (ShipmentFinderColumnName)column.Index);
            };
        }

        public override List<byte[]> SaveOLVStates()
        {
            return new List<byte[]> { shipmentFinderObjectListView.SaveState() };
        }

        public override bool RestoreOLVStates(List<byte[]> state)
        {
            if ( state?.Count == 1)
            {
                return shipmentFinderObjectListView.RestoreState(state[0]);
            }

            return false;
        }

        public override async Task RefreshDataAsync()
        {
            await RunOnUIThread(() =>
            {
                searchButton.Enabled = false;
                shipmentFinderObjectListView.ClearObjects();
                shipmentFinderObjectListView.EmptyListMsg = "Loading...";
                shipmentFinderObjectListView.Refresh();
            });

            ShipmentModels = new List<ShipmentModel>();
            if (!String.IsNullOrWhiteSpace(planetTextBox.Text))
            {
                string planet = planetTextBox.Text;
                string endpoint = originRadioButton.Checked ? $"/localmarket/shipping/source/{planet}" : $"/localmarket/shipping/destination/{planet}";
                using (GetRequest searchRequest = new GetRequest(endpoint, Main.Auth.AuthToken))
                {
                    ShipmentModels = await searchRequest.GetResponseAsync<List<ShipmentModel>>();
                }
            }

            await RunOnUIThread(() =>
            {
                if ( ShipmentModels != null )
                {
                    shipmentFinderObjectListView.SetObjects(ShipmentModels);
                }

                shipmentFinderObjectListView.EmptyListMsg = "No data.";
                searchButton.Enabled = true;
            });
        }

        private async void searchButton_Click(object sender, EventArgs e)
        {
            await RefreshDataAsync();
        }
    }
}
