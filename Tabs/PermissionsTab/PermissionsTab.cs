﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Forms;

using FIOUI.Web;

using Newtonsoft.Json;

namespace FIOUI.Tabs.PermissionsTab
{
    public partial class PermissionsTab : FIOTab
    {
        private List<PermissionModel> PermissionModels = null;

        public PermissionsTab()
        {
            InitializeComponent();
        }

        public override List<byte[]> SaveOLVStates()
        {
            return new List<byte[]> { permissionsListView.SaveState() };
        }

        public override bool RestoreOLVStates(List<byte[]> state)
        {
            if(state?.Count == 1)
            {
                return permissionsListView.RestoreState(state[0]);
            }

            return false;
        }

        private void SetUIEnabledState(bool bEnabled)
        {
            addPermissionButton.Enabled = bEnabled;
            removePermissionButton.Enabled = bEnabled;
            applyChangesButton.Enabled = bEnabled;
        }

        public override async Task RefreshDataAsync()
        {
            await RunOnUIThread(() =>
            {
                SetUIEnabledState(false);

                permissionsListView.ClearObjects();
                permissionsListView.EmptyListMsg = "Loading...";
                permissionsListView.Refresh();
            });

            using (GetRequest permissionRequest = new GetRequest($"/auth/permissions", Main.Auth.AuthToken))
            {
                PermissionModels = await permissionRequest.GetResponseAsync<List<PermissionModel>>();
            }

            await RunOnUIThread(() =>
            {
                if ( PermissionModels != null )
                {
                    permissionsListView.SetObjects(PermissionModels);
                }

                permissionsListView.EmptyListMsg = "No entries.";

                SetUIEnabledState(true);
            });

            await Task.FromResult(0);
        }

        private async void applyChangesButton_Click(object sender, EventArgs e)
        {
            SetUIEnabledState(false);

            bool bAtLeastOneFailure = false;
            for( int i = PermissionModels.Count - 1; i >= 0; --i)
            {
                var perm = PermissionModels[i];

                PostRequest request = new PostRequest("/auth/addpermission", Main.Auth.AuthToken, JsonConvert.SerializeObject(perm));
                await request.GetResultNoResponseAsync();
                if ( request.StatusCode != HttpStatusCode.OK )
                {
                    bAtLeastOneFailure = true;
                    PermissionModels.RemoveAt(i);
                }
            }

            if (bAtLeastOneFailure)
            {
                MessageBox.Show("Applied, but an entry failed to be applied.\nMaybe the User doesn't exist?");
                permissionsListView.SetObjects(PermissionModels);
            }
            else
            {
                MessageBox.Show("Applied");
            }

            SetUIEnabledState(true);
        }

        private void addPermissionButton_Click(object sender, EventArgs e)
        {
            PermissionModel newPerm = new PermissionModel();
            newPerm.UserName = "Double-click here";
            newPerm.FlightData = true;
            newPerm.BuildingData = true;
            newPerm.StorageData = true;
            newPerm.ProductionData = true;
            newPerm.WorkforceData = true;
            newPerm.ExpertsData = true;

            PermissionModels.Add(newPerm);
            permissionsListView.SetObjects(PermissionModels);
        }

        private async void PermissionsTab_Load(object sender, EventArgs e)
        {
            await RunOnUIThread(() =>
            {
                Text = "Permissions";
            });
        }

        private void PermissionsTab_SelectedIndexChanged(object sender, EventArgs e)
        {
            removePermissionButton.Enabled = permissionsListView.SelectedIndex >= 0;
        }

        private async void removePermissionButton_Click(object sender, EventArgs e)
        {
            SetUIEnabledState(false);

            PermissionModel SelectedPerm = permissionsListView.SelectedObject as PermissionModel;
            if ( SelectedPerm != null )
            {
                PostRequest request = new PostRequest($"/auth/deletepermission/{SelectedPerm.UserName}", Main.Auth.AuthToken, null);
                await request.GetResultNoResponseAsync();
                permissionsListView.RemoveObject(SelectedPerm);
            }

            SetUIEnabledState(true);
        }
    }
}
