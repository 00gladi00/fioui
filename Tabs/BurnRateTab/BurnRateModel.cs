﻿using System.Collections.Generic;

namespace FIOUI.Tabs.BurnRateTab
{
    public class BurnRateModel
    {
        public string PlanetId { get; set; }
        public string PlanetName { get; set; }
        public string PlanetNaturalId { get; set; }
        public string Material { get; set; }
        public float? BurnRate { get; set; }
        public int? UnitsOnPlanet { get; set; }
        public float? DaysRemaining { get; set; }
        public string DaysRemainingDisplay
        {
            get
            {
                return DaysRemaining?.ToString("N2");
            }
        }

        public string PlanetDisplayName
        {
            get
            {
                return (PlanetNaturalId != PlanetName) ? $"{PlanetName} ({PlanetNaturalId})" : PlanetNaturalId;
            }
        }

        public List<BurnRateModel> Children { get; set; } = new List<BurnRateModel>();
    }
}
