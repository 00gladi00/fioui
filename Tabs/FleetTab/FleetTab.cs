﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

using Newtonsoft.Json;

using FIOUI.JsonRepresentations;
using FIOUI.Web;
using SharpGL;

namespace FIOUI.Tabs.FleetTab
{
    public partial class FleetTab : FIOTab
    {
        private List<FleetModel> FleetModels = null;
        private List<string> UsersChecked = null;

        public FleetTab()
        {
            InitializeComponent();

            Text = "Fleet";
        }

        private async void FleetTab_Load(object sender, EventArgs e)
        {
            _ = initializeOpenGLInfo();
            openGLSceneControl.MouseWheel += openGLSceneControl_MouseWheel;

            await Task.FromResult(0);
        }

        public override List<byte[]> SaveOLVStates()
        {
            return new List<byte[]> { flightsObjectListView.SaveState(), segmentsObjectListView.SaveState() };
        }

        public override bool RestoreOLVStates(List<byte[]> state)
        {
            if (state?.Count == 2)
            {
                bool bRestored = true;
                bRestored &= flightsObjectListView.RestoreState(state[0]);
                bRestored &= segmentsObjectListView.RestoreState(state[1]);
                return bRestored;
            }

            return false;
        }

        public override string SaveGeneralState()
        {
            return JsonConvert.SerializeObject(usersCheckedListBox.CheckedItems.OfType<string>().ToList());
        }

        public override bool RestoreGeneralState(string state)
        {
            if (!String.IsNullOrWhiteSpace(state))
            {
                var generalState = JsonConvert.DeserializeObject<List<string>>(state);
                if (generalState != null)
                {
                    UsersChecked = generalState;
                }
            }

            return UsersChecked != null;
        }

        private async Task PopulateUsers()
        {
            if (usersCheckedListBox.Items.Count == 0)
            {
                List<string> flightUsers = null;
                using(var getFlightUsers = new GetRequest("/auth/visibility/flight", Main.Auth.AuthToken))
                {
                    flightUsers = await getFlightUsers.GetResponseAsync<List<string>>();
                }

                int comboBoxIdx = 0;
                foreach( var user in flightUsers)
                {
                    bool bChecked = false;
                    if ( UsersChecked != null )
                    {
                        bChecked = UsersChecked.Contains(user);
                    }

                    usersCheckedListBox.Items.Add(user);
                    usersCheckedListBox.SetItemChecked(comboBoxIdx++, bChecked);
                }
            }
        }

        public override async Task RefreshDataAsync()
        {
            await RunOnUIThread(() =>
            {
                refreshButton.Enabled = false;
                usersCheckedListBox.Enabled = false;

                segmentsObjectListView.ClearObjects();
                segmentsObjectListView.EmptyListMsg = "No flight selected.";

                flightsObjectListView.ClearObjects();
                flightsObjectListView.EmptyListMsg = "Loading...";
                flightsObjectListView.Refresh();
            });

            FleetModels = new List<FleetModel>();

            await PopulateUsers();

            var fuels = await DataCache.GetFuels();
            var sfMat = fuels.Where(f => f.Ticker == "SF").FirstOrDefault();
            var ffMat = fuels.Where(f => f.Ticker == "FF").FirstOrDefault();

            List<string> usersChecked = usersCheckedListBox.CheckedItems.OfType<string>().ToList();
            foreach(var user in usersChecked)
            {
                using(var getShips = new GetRequest($"/ship/ships/{user}", Main.Auth.AuthToken))
                using(var getFlights = new GetRequest($"/ship/flights/{user}", Main.Auth.AuthToken))
                {
                    var ships = await getShips.GetResponseAsync<JsonShips>();
                    var flights = await getFlights.GetResponseAsync<JsonFlights>();

                    if ( ships != null && flights != null )
                    {
                        DateTime lastShipsUpdate = ships.Timestamp;

                        List<JsonRepresentations.Storage.Rootobject> fuelStores = null;
                        using (var getFuel = new GetRequest($"/ship/ships/fuel/{user}", Main.Auth.AuthToken))
                        {
                            fuelStores = await getFuel.GetResponseAsync<List<JsonRepresentations.Storage.Rootobject>>();
                        }

                        // Create models.  Add them.
                        foreach (var ship in ships.Ships)
                        {
                            Flight fl = flights.Flights.Where(f => f.ShipId == ship.ShipId).FirstOrDefault();

                            FleetModel fm = new FleetModel();

                            fm.UserName = user;
                            fm.ShipRegistration = ship.Registration;
                            fm.ShipName = ship.Name;
                            fm.Location = ship.Location;

                            using (var getSF = new GetRequest($"/storage/{ships.UserNameSubmitted}/{ship.StlFuelStoreId}", Main.Auth.AuthToken))
                            using (var getFF = new GetRequest($"/storage/{ships.UserNameSubmitted}/{ship.FtlFuelStoreId}", Main.Auth.AuthToken))
                            {
                                var sfStore = await getSF.GetResponseAsync<JsonRepresentations.Storage.Rootobject>();
                                var ffStore = await getFF.GetResponseAsync<JsonRepresentations.Storage.Rootobject>();

                                if (sfStore != null)
                                {
                                    fm.SF = Math.Round(sfStore.WeightLoad / sfMat.Weight);
                                    fm.SFMax = Math.Round(sfStore.WeightCapacity / sfMat.Weight);
                                }

                                if (ffStore != null)
                                {
                                    fm.FF = Math.Round(ffStore.WeightLoad / ffMat.Weight);
                                    fm.FFMax = Math.Round(ffStore.WeightCapacity / ffMat.Weight);
                                }
                            }

                            fm.UpdateTime = ships.Timestamp;

                            if (fl != null)
                            {
                                fm.CurrentSegmentIndex = fl.CurrentSegmentIndex;

                                fm.Origin = fl.Origin;
                                fm.Destination = fl.Destination;

                                fm.ETA = Utils.FromUnixTime(fl.ArrivalTimeEpochMs).ToLocalTime();

                                for(int segmentIdx = 0; segmentIdx < fl.Segments.Count; ++segmentIdx)
                                {
                                    var segment = fl.Segments[segmentIdx];

                                    Segment seg = new Segment();

                                    seg.IsSegmentDone = (segmentIdx < fm.CurrentSegmentIndex);
                                    seg.IsSegmentCurrent = (segmentIdx == fm.CurrentSegmentIndex);
                                    seg.Type = segment.Type;
                                    seg.Origin = segment.Origin;
                                    seg.DepartureTime = Utils.FromUnixTime(segment.DepartureTimeEpochMs).ToLocalTime();
                                    seg.Destination = segment.Destination;
                                    seg.ArrivalTime = Utils.FromUnixTime(segment.ArrivalTimeEpochMs).ToLocalTime();
                                    seg.StlDistance = segment.StlDistance;
                                    seg.StlFuelConsumption = segment.StlFuelConsumption;
                                    seg.FtlDistance = segment.FtlDistance;
                                    seg.FtlFuelConsumption = segment.FtlFuelConsumption;

                                    foreach(var originLine in segment.OriginLines)
                                    {
                                        Line l = new Line();
                                        l.Type = originLine.Type;
                                        l.LineId = originLine.LineId;
                                        l.LineName = originLine.LineName;
                                        l.LineNaturalId = originLine.LineNaturalId;
                                        seg.OriginLines.Add(l);
                                    }
                                    foreach(var destinationLine in segment.DestinationLines)
                                    {
                                        Line l = new Line();
                                        l.Type = destinationLine.Type;
                                        l.LineId = destinationLine.LineId;
                                        l.LineName = destinationLine.LineName;
                                        l.LineNaturalId = destinationLine.LineNaturalId;
                                        seg.DestinationLines.Add(l);
                                    }

                                    fm.Segments.Add(seg);
                                }
                            }

                            FleetModels.Add(fm);
                        }
                    }
                }
            }

            await RunOnUIThread(() =>
            {
                refreshButton.Enabled = true;
                usersCheckedListBox.Enabled = true;

                flightsObjectListView.EmptyListMsg = "No data.";
                flightsObjectListView.SetObjects(FleetModels);
            });
        }

        private void refreshButton_Click(object sender, EventArgs e)
        {
            _ = RefreshDataAsync();
        }

        private FleetModel SelectedFleetModel = null;
        private void flightsObjectListView_SelectionChanged(object sender, EventArgs e)
        {
            FleetModel model = flightsObjectListView.SelectedObject as FleetModel;
            if (model != null)
            {
                SelectedFleetModel = model;
                segmentsObjectListView.EmptyListMsg = "Not currently in flight.";
                segmentsObjectListView.SetObjects(model.Segments);
            }
            else
            {
                segmentsObjectListView.EmptyListMsg = "No flight selected.";
                SelectedFleetModel = null;
            }

            NotifyFleetModelChanged();
        }

        private void segmentsObjectListView_FormatRow(object sender, BrightIdeasSoftware.FormatRowEventArgs e)
        {
            Segment model = (Segment)e.Model;
            if (model.IsSegmentDone)
            {
                e.Item.BackColor = Color.LightGray;
            }
            else if (model.IsSegmentCurrent)
            {
                e.Item.BackColor = Color.LightGreen;
            }
        }
    }
}
