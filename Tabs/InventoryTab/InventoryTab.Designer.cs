﻿
namespace FIOUI.Tabs.InventoryTab
{
    partial class InventoryTab
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InventoryTab));
            this.inventoryTreeListView = new BrightIdeasSoftware.TreeListView();
            this.olvColumn1 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn2 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn3 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            ((System.ComponentModel.ISupportInitialize)(this.inventoryTreeListView)).BeginInit();
            this.SuspendLayout();
            // 
            // inventoryTreeListView
            // 
            this.inventoryTreeListView.AllColumns.Add(this.olvColumn1);
            this.inventoryTreeListView.AllColumns.Add(this.olvColumn2);
            this.inventoryTreeListView.AllColumns.Add(this.olvColumn3);
            this.inventoryTreeListView.AlternateRowBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.inventoryTreeListView.CellEditUseWholeCell = false;
            this.inventoryTreeListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvColumn1,
            this.olvColumn2,
            this.olvColumn3});
            this.inventoryTreeListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inventoryTreeListView.HideSelection = false;
            this.inventoryTreeListView.Location = new System.Drawing.Point(0, 0);
            this.inventoryTreeListView.Name = "inventoryTreeListView";
            this.inventoryTreeListView.ShowGroups = false;
            this.inventoryTreeListView.Size = new System.Drawing.Size(800, 450);
            this.inventoryTreeListView.TabIndex = 0;
            this.inventoryTreeListView.UseAlternatingBackColors = true;
            this.inventoryTreeListView.UseCompatibleStateImageBehavior = false;
            this.inventoryTreeListView.View = System.Windows.Forms.View.Details;
            this.inventoryTreeListView.VirtualMode = true;
            // 
            // olvColumn1
            // 
            this.olvColumn1.AspectName = "PlanetDisplayName";
            this.olvColumn1.Text = "Planet";
            this.olvColumn1.Width = 200;
            // 
            // olvColumn2
            // 
            this.olvColumn2.AspectName = "MaterialName";
            this.olvColumn2.Text = "Material";
            this.olvColumn2.Width = 100;
            // 
            // olvColumn3
            // 
            this.olvColumn3.AspectName = "UnitsOnPlanet";
            this.olvColumn3.Text = "Units";
            // 
            // InventoryTab
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.inventoryTreeListView);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "InventoryTab";
            this.Text = "InventoryTab";
            this.Load += new System.EventHandler(this.InventoryTab_Load);
            ((System.ComponentModel.ISupportInitialize)(this.inventoryTreeListView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private BrightIdeasSoftware.TreeListView inventoryTreeListView;
        private BrightIdeasSoftware.OLVColumn olvColumn1;
        private BrightIdeasSoftware.OLVColumn olvColumn2;
        private BrightIdeasSoftware.OLVColumn olvColumn3;
    }
}