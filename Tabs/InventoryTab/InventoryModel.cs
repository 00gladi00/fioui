﻿using System.Collections.Generic;

namespace FIOUI.Tabs.InventoryTab
{
    public class InventoryModel
    {
        public string PlanetId { get; set; }
        public string PlanetName { get; set; }
        public string PlanetNaturalId { get; set; }
        public string MaterialName { get; set; }
        public int? UnitsOnPlanet { get; set; }

        public string PlanetDisplayName
        {
            get
            {
                return (PlanetNaturalId != PlanetName) ? $"{PlanetName} ({PlanetNaturalId})" : PlanetNaturalId;
            }
        }

        public List<InventoryModel> Children { get; set; } = new List<InventoryModel>();
    }
}
