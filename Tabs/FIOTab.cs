﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

using FIOUI.Dialogs;
using FIOUI.Web;

using WeifenLuo.WinFormsUI.Docking;

namespace FIOUI.Tabs
{
    public class FIOTab : DockContent
    {
        protected TaskScheduler UISyncContext = null;

        public static List<FIOTab> OpenFIOTabs { get; set; } = new List<FIOTab>();

        public FIOTab() : base()
        {
            KeyPreview = true;
            KeyDown += FIOTab_KeyDown;

            Load += FIOTab_Load;
            FormClosing += FIOTab_FormClosing;
            FormClosed += FIOTab_FormClosed;

            UISyncContext = TaskScheduler.FromCurrentSynchronizationContext();
        }

        protected override string GetPersistString()
        {
            return $"{GetType()},{DataUserName}";
        }

        public string DataUserName
        {
            get; set;
        } = Main.Auth?.UserName;

        public virtual bool CanUseWithOtherUsers
        {
            get
            {
                return false;
            }
        }

        public virtual List<string> PermissionTypes
        {
            get; set;
        } = new List<string>();

        private async Task<List<string>> GetUsersAllowedToSeePermissions()
        {
            List<string> users = new List<string>();
            if (PermissionTypes.Count > 0)
            {
                using (GetRequest permReq = new GetRequest($"/auth/visibility/{PermissionTypes[0]}", Main.Auth.AuthToken))
                {
                    users = await permReq.GetResponseAsync<List<string>>();
                    foreach (var permissionType in PermissionTypes.Skip(1))
                    {
                        using (GetRequest perPermReq = new GetRequest($"/auth/visibility/{permissionType}", Main.Auth.AuthToken))
                        {
                            List<string> perPermRes = await perPermReq.GetResponseAsync<List<string>>();
                            users = users.Intersect(perPermRes).ToList();
                        }
                    }
                }
            }

            return users;
        }

        public async Task<bool> PreCreateRestoreCheck()
        {
            if (!CanUseWithOtherUsers || DataUserName == Main.Auth.UserName)
            {
                // Restore = restoring settings
                return true;
            }

            List<string> usersAllowedToSee = await GetUsersAllowedToSeePermissions();
            return usersAllowedToSee.Count > 0;
        }

        /// <summary>
        /// Run prior to creation.  
        /// Should be used for prompting the user for additional data for the tab.
        /// </summary>
        /// <returns></returns>
        public async Task<bool> PreCreateCheck()
        {
            if (PermissionTypes.Count == 0 || !CanUseWithOtherUsers )
            {
                return false;
            }

            List<string> usersAllowedToSee = await GetUsersAllowedToSeePermissions();
            if ( usersAllowedToSee.Count > 0 )
            {
                DropboxDlg dlg = new DropboxDlg("Select a User", usersAllowedToSee);
                dlg.ShowDialog();
                if (dlg.SelectedOption != null)
                {
                    DataUserName = dlg.SelectedOption;
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                MessageBox.Show("No users have permitted you to view information for this window type.");
                return false;
            }
        }

        /// <summary>
        /// Notifier for Refreshing the data on this tab
        /// </summary>
        public virtual async Task RefreshDataAsync()
        {
            await Task.FromResult(0);
        }

        #region GeneralState
        public virtual bool RestoreGeneralState(string state)
        {
            return true;
        }

        public virtual string SaveGeneralState()
        {
            return null;
        }
        #endregion

        #region OLVState
        public virtual bool RestoreOLVStates(List<byte[]> state)
        {
            return true;
        }

        public virtual List<byte[]> SaveOLVStates()
        {
            return null;
        }
        #endregion

        protected Task RunOnUIThread(Action action)
        {
            return Task.Factory.StartNew(action, CancellationToken.None, TaskCreationOptions.None, UISyncContext);
        }

        private void FIOTab_Load(object sender, EventArgs e)
        {
            OpenFIOTabs.Add(this);
        }

        private async void FIOTab_KeyDown(object sender, KeyEventArgs e)
        {
            if ( e.KeyCode == Keys.F5)
            {
                await RefreshDataAsync();
            }
        }

        private void FIOTab_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void FIOTab_FormClosed(object sender, FormClosedEventArgs e)
        {
            OpenFIOTabs.Remove(this);
        }
    }
}
