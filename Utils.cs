﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace FIOUI
{
	public static class Utils
	{
		public static void LaunchURI(string URI)
		{
			try
			{
				Process.Start(URI);
			}
			catch (System.ComponentModel.Win32Exception)
			{
				Process.Start("IExplore.exe", URI);
			}
			catch
			{

			}
		}

		private static string ClipboardSetFailureMsg = "Failed to set data on Clipboard." + Environment.NewLine + Environment.NewLine + "This can sometimes happen when other applications prevent it from happening.";
		public static bool SetClipboardText(string text)
		{
			try
			{
				Clipboard.SetText(text);
				return true;
			}
			catch (Exception)
			{
				MessageBox.Show(ClipboardSetFailureMsg);
				return false;
			}
		}

		private static readonly DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
		public static DateTime FromUnixTime(long unixTime)
		{
			return epoch.AddMilliseconds(unixTime);
		}
	}
}
