@echo off
SETLOCAL ENABLEDELAYEDEXPANSION

REM Get Latest Version
SET /p ExistingVersion=<..\FIORest\FIOUIRelease\Latest.txt

REM Get Latest Version
powershell -Command "[System.Diagnostics.FileVersionInfo]::GetVersionInfo("""bin\Release\FIOUI.exe""").FileVersion.ToString()" > latestVersion.txt
SET /p LatestVersion=<latestVersion.txt
DEL latestVersion.txt

IF %ExistingVersion% == %LatestVersion% GOTO :sameversion

REM @echo Copying Setup Executable...
COPY /Y FIOUI-Setup.exe ..\FIORest\FIOUIRelease\FIOUI-Setup.exe

REM @echo Copying Release Notes...
COPY /Y ReleaseNotes.rtf ..\FIORest\FIOUIRelease\ReleaseNotes.rtf

REM @echo Copying Extension...
COPY /Y ..\FIOExtension.zip ..\FIORest\FIOUIRelease\FIOExtension.zip

REM @echo Creating version file...
@echo %LatestVersion% > ..\FIORest\FIOUIRelease\Latest.txt

@echo Successfully deployed
pause
GOTO :eof

:sameversion
@ECHO The version either has not been rebuilt or needs its version bumped.
pause
GOTO :failure

:failure
CMD /c EXIT 1

:eof