﻿using System;

namespace FIOUI.JsonRepresentations
{
    public class JsonAuthLoginPayload
    {
        public string AuthToken { get; set; }
        public DateTime Expiry { get; set; }
    }
}
