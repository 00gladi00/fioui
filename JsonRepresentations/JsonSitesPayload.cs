﻿using System;
using System.Collections.Generic;

namespace FIOUI.JsonRepresentations
{
    public class SITESPayload
    {
        public List<SITESSite> Sites { get; set; } = new List<SITESSite>();

        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class SITESSite
    {
        public string SiteId { get; set; }

        public string PlanetId { get; set; }
        public string PlanetIdentifier { get; set; }
        public string PlanetName { get; set; }

        public long PlanetFoundedEpochMs { get; set; }

        public List<SITESBuilding> Buildings { get; set; } = new List<SITESBuilding>();
    }

    public class SITESBuilding
    {
        public string BuildingName { get; set; }
        public string BuildingTicker { get; set; }

        public double Condition { get; set; }
        public List<SITESReclaimableMaterial> ReclaimableMaterials { get; set; } = new List<SITESReclaimableMaterial>();
        public List<SITESRepairMaterial> RepairMaterials { get; set; } = new List<SITESRepairMaterial>();
    }

    public class SITESReclaimableMaterial
    {
        public string MaterialId { get; set; }
        public string MaterialName { get; set; }
        public string MaterialTicker { get; set; }

        public int MaterialAmount { get; set; }
    }

    public class SITESRepairMaterial
    {
        public string MaterialId { get; set; }
        public string MaterialName { get; set; }
        public string MaterialTicker { get; set; }

        public int MaterialAmount { get; set; }
    }
}
