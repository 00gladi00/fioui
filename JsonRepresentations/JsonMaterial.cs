﻿using System;

namespace FIOUI.JsonRepresentations
{
    public class Material
    {
        public string CategoryName { get; set; }
        public string CategoryId { get; set; }
        public string Name { get; set; }
        public string Id { get; set; }
        public string Ticker { get; set; }
        public float Weight { get; set; }
        public float Volume { get; set; }
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
