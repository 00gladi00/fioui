﻿using System;
using System.Collections.Generic;

namespace FIOUI.JsonRepresentations
{
    public class WorldSectors
    {
        public string SectorId { get; set; }
        public string Name { get; set; }

        public int HexQ { get; set; }
        public int HexR { get; set; }
        public int HexS { get; set; }

        public int Size { get; set; }

        public List<SubSector> SubSectors { get; set; } = new List<SubSector>();

        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class SubSector
    {
        public string Id { get; set; }

        public List<SubSectorVertex> Vertices { get; set; } = new List<SubSectorVertex>();
    }

    public class SubSectorVertex
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }
    }
}
